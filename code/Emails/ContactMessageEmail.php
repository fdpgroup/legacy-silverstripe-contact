<?php


class ContactMessageEmail extends Email {

    public static function create() {
        $args = func_get_args();
        $controller = $args[0];
        $message = $args[1];
        $subject = Config::inst()->get(get_class($controller), 'contact_notification_email_subject');
        if (Config::inst()->get(get_class($controller), 'contact_form_translations')) {
            $subject = _t($subject);
        }
        $subject = $message->formatString($subject);
        $email = new ContactMessageEmail(
            $controller->FDPContact_Sender,
			$controller->FDPContact_Recipients,
            $subject
        );
        $email->setTemplate('ContactMessageEmail');
        $email->populateTemplate(ArrayData::create(array(
            'Subject' => $subject,
            'Message' => $message->getEmailData()
        )));
		return $email;
    }
}
