<?php


class ContactMessageThankYouEmail extends Email {

    public static function create() {
        $args = func_get_args();
        $controller = $args[0];
        $message = $args[1];
        $recipient_field = Config::inst()->get(get_class($controller), 'contact_thank_you_email_recipient');
        $subject = Config::inst()->get(get_class($controller), 'contact_thank_you_email_subject');
        if (Config::inst()->get(get_class($controller), 'contact_form_translations')) {
            $subject = _t($subject);
        }
        $subject = $message->formatString($subject);
        if (!empty($recipient_field)) {
            $email = new ContactMessageThankYouEmail(
                $controller->FDPContact_Sender,
    			$message->$recipient_field,
                $subject
            );
            $email->setTemplate('ContactMessageEmail');
            $email->populateTemplate(ArrayData::create(array(
                'Subject' => $subject,
                'Message' => $message->getEmailData()
            )));
    		return $email;
        }
        else {
            return null;
        }
    }
}
