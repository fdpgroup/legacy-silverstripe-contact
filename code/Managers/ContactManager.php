<?php


class ContactManager extends ModelAdmin {

    private static $menu_title = 'Messages';
    private static $url_segment = 'messages';
	private static $menu_icon = 'fdp-contact/images/ContactManager.png';
    private static $managed_models = array(
        'ContactMessage' => array(
            'title' => 'Messages'
        )
    );
    private static $model_importers = array();

    public function getList() {
        $list = parent::getList();
        if ($this->modelClass == 'ContactMessage') {
            $list = $list->sort('Created DESC');
        }
        return $list;
    }

    public function getEditForm($id=null, $fields=null) {
        $form = parent::getEditForm($id, $fields);

        if (($field = $form->Fields()->dataFieldByName('ContactMessage')) && $field instanceof GridField) {
            $config = $field->getConfig();
            $config->getComponentByType('GridFieldExportButton')->setExportColumns(
                Config::inst()->get('ContactMessage', 'export_columns')
            );
        }

        return $form;
    }
}
