<?php


class FDPContact_ContactPage extends DataExtension {

    private static $db = array(
        'FDPContact_Sender' => 'Varchar(254)',
        'FDPContact_Recipients' => 'Text',
    	'FDPContact_ThankYouMessage' => 'HTMLText'
    );
    private static $has_one = array(
        'FDPContact_ThankYouPage' => 'SiteTree'
    );

    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldToTab(
			'Root.Contact',
			TextField::create('FDPContact_Sender', 'Enquiry Emails Sender Address')
		);
    	$fields->addFieldToTab(
			'Root.Contact',
			TextField::create('FDPContact_Recipients', 'Recipients of Enquiry Emails (comma separated list)')
		);
        $fields->addFieldToTab(
            'Root.Contact',
            TreeDropdownField::create('FDPContact_ThankYouPageID', 'Thank You Page', 'SiteTree')
        );
    	$fields->addFieldToTab(
			'Root.Contact',
			HTMLEditorField::create('FDPContact_ThankYouMessage', 'Thank You Message')
		);
	}

    private $_form_popup = null;

    public function FormPopup() {
        if (is_null($this->_form_popup)) {
            Requirements::css('fdp-contact/css/contact-popup.css');
            if (Config::inst()->get(get_class($this->owner), 'popup_load_jquery')) {
                Requirements::javascript('fdp-contact/javascript/jquery-3.2.1.min.js');
            }
            if (Config::inst()->get(get_class($this->owner), 'popup_load_magnific')) {
                Requirements::javascript('fdp-contact/javascript/jquery.magnific-popup.min.js');
            }
            Requirements::javascript('fdp-contact/javascript/FormPopup.js');
            $this->_form_popup = ArrayData::create(array(
                'Link' => $this->owner->Link(),
                'PopupAttrs' => sprintf('data-popup-url="%s"', $this->owner->Link('/popup'))
            ));
        }
        return $this->_form_popup;
    }
}


class FDPContact_ContactPage_Controller extends Extension {

	private static $_session_key = 'FDPContact_Sent%d';

    private static $allowed_actions = array(
        'ContactForm',
        'Popup'
    );
	private static $url_handlers = array(
		'ContactForm' => 'ContactForm',
        'popup' => 'Popup'
	);

	public function ContactForm() {
        return ContactForm::create($this->owner);
    }

    public function SendMessage($data, $form) {
        $message = ContactMessage::create(array(
			'PageID' => $this->owner->ID
		));
        $form->saveInto($message);
        $message->write();

		$notification = ContactMessageEmail::create($this->owner, $message);
        $notification->send();

        $thanks = ContactMessageThankYouEmail::create($this->owner, $message);
        if (!is_null($thanks)) {
            $thanks->send();
        }

        $return_url = null;
        if ($this->owner->FDPContact_ThankYouPageID > 0 && ($redirect = $this->owner->FDPContact_ThankYouPage())) {
            $return_url = $redirect->Link();
        }

        if ($this->owner->getRequest()->getVar('ajax') == 1) {
            if (!is_null($return_url)) {
                $response = new SS_HTTPResponse(Convert::array2json(array(
                    'Status' => 'OK',
                    'ThankYouURL' => $return_url
                )));
            }
            else {
    			$response = new SS_HTTPResponse(Convert::array2json(array(
    				'Status' => 'OK',
    				'ThankYouMessage' => $this->owner->FDPContact_ThankYouMessage
    			)));
            }
			$response->addHeader('Content-Type', 'application/json');
			return $response;
		}
		else {
            if (!is_null($return_url)) {
                return $this->owner->redirect($return_url);
            }
            else {
                Session::set(sprintf(self::$_session_key, $this->owner->ID), 1);
        		return $this->owner->redirect($this->owner->Link());
            }
        }
    }

    public function Sent() {
        if (Session::get(sprintf(self::$_session_key, $this->owner->ID)) == 1) {
			Session::clear(sprintf(self::$_session_key, $this->owner->ID));
			return true;
		}
		else {
			if ($this->owner->request->getVar('sent') == 1) {
				return true;
			}
			else {
				return false;
			}
		}
    }

    public function Popup() {
		return $this->owner->renderWith('ContactFormPopup');
	}
}
