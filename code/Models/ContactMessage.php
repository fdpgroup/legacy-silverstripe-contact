<?php


class ContactMessage extends DataObject {

    private static $has_one = array(
        'Page' => 'SiteTree'
    );
    private static $default_sort = 'Created DESC';

    public function canCreate($member = null) {
		return false;
	}

    public function canEdit($member = null) {
		return false;
	}

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->insertAfter(
            DateTimeField::create('Created', 'Date/Time Sent', $this->Created),
            'PageID'
        );
        return $fields;
    }

    public function formatString($string) {
        $fields = Config::inst()->get(get_class($this), 'db');
        if (preg_match_all('/\{\$([A-Za-z0-9\_]*?)\}/', $string, $matches)) {
            foreach ($matches[1] as $variable) {
                $token = sprintf('{$%s}', $variable);
                if (array_key_exists($variable, $fields)) {
                    $string = str_replace($token, $this->$variable, $string);
                }
                else {
                    $string = str_replace($token, '', $string);
                }
            }
        }
        return $string;
    }

    private $_email_data;

    public function getEmailData() {
        $fields = Config::inst()->get(get_class($this), 'db');
        if (is_null($this->_email_data)) {
            $this->_email_data = ArrayList::create();
            foreach ($fields as $name => $type) {
                if ($value = $this->$name) {
                    $this->_email_data->push(ArrayData::create(array(
                        'Label' => FormField::name_to_label($name),
                        'Value' => $value
                    )));
                }
            }
        }
        return $this->_email_data;
    }
}
