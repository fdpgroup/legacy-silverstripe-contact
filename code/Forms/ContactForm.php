<?php


class ContactForm extends Form {

    public static function create() {
        $args = func_get_args();
        $controller = $args[0];
        $fields = FieldList::create();

        $translate = Config::inst()->get(get_class($controller), 'contact_form_translations');

        $required = array();
        foreach (Config::inst()->get(get_class($controller), 'contact_form_fields') as $field) {
            $type = $field['Type'];
            $label = array_key_exists('Label', $field) ? $field['Label'] : $field['Name'];
            if ($translate) {
                $label = _t($label);
            }
            $field_obj = new $type($field['Name'], $label);
            switch ($type) {
                case 'DateField':
                    $field_obj->setConfig('datavalueformat', 'yyyy-MM-dd');
                    $field_obj->setConfig('dateformat', 'dd/MM/yyyy');
                    break;
                case 'DropdownField':
                case 'OptionSetField':
                case 'CheckboxSetField':
                    if (array_key_exists('Options', $field) && ($options = $field['Options'])) {
                        if ($options == 'EnumValues') {
                            $options = singleton('ContactMessage')->dbObject($field['Name'])->enumValues();
                        }
                        else {
                            if ($translate) {
                                $translated_options = array();
                                foreach ($options as $k => $v) {
                                    $translated_options[_t($k)] = _t($v);
                                }
                                $options = $translated_options;
                            }
                        }
                        $field_obj->setSource($options);
                    }
                    break;
            }
            $fields->push($field_obj);
            if (array_key_exists('Required', $field) && $field['Required']) {
                $required[] = $field['Name'];
            }
        }
        if ($send_text = Config::inst()->get(get_class($controller), 'contact_form_send_text')) {
            if ($translate) {
                $send_text = _t($send_text);
            }
        }
        else {
            $send_text = 'Send';
        }
        $form = new ContactForm(
            $controller,
            'ContactForm',
            $fields,
            new FieldList(
                FormAction::create('SendMessage', $send_text)->setUseButtonTag(true)->setButtonContent("<span>{$send_text}</span>")
            ),
            new RequiredFields($required)
        );
        $form->setAttribute('novalidate', 'novalidate');
        $form->setTemplate('ContactForm');
        $form->addExtraClass('fdp-contact-form');
        Session::clear(sprintf('FormInfo.%s', $form->FormName()));
        return $form;
    }

    public function forTemplate() {
        Requirements::css('fdp-contact/css/contact.css');
        return parent::forTemplate();
    }

    protected function getValidationErrorResponse() {
		$request = $this->getRequest();
		if ($request->getVar('ajax') == 1) {
			$errors = array();
			foreach ($this->validator->getErrors() as $error) {
				$errors[] = array(
					'Name' => $error['fieldName'],
					'Message' => $error['message']
				);
			}
			$response = new SS_HTTPResponse(Convert::array2json(array(
				'Status' => 'INVALID',
				'Errors' => $errors
			)));
			$response->addHeader('Content-Type', 'application/json');
			return $response;
		}
		return parent::getValidationErrorResponse();
	}
}
