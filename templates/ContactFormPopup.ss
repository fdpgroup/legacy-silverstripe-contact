<div class="popup form">
	<h2>$Title</h2>
    <div class="form-content">
    	<% if not $Sent %>
    		$Content
    		$ContactForm
    	<% else %>
    		$FDPContact_ThankYouMessage
    	<% end_if %>
    </div>
	<div class="clearer"></div>
</div>
