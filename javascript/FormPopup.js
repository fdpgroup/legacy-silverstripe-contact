$popup = jQuery.noConflict();

$popup(document).ready(function() {
    $popup('a[data-popup-url]').click(function(e) {
        e.preventDefault();
        $popup.magnificPopup.open({
    		items: {
    			type: 'ajax',
    			src: $popup(this).attr('data-popup-url')
    		},
    		ajax: {
    			cache: false
    		},
    		callbacks: {
                parseAjax: function(response) {
                    include_css = response.xhr.getResponseHeader('X-Include-CSS');
                    if (include_css != null) {
                        files = include_css.split(',');
                        for (i = 0; i < files.length; i++) {
                            search = files[i];
                            querystring = files[i].lastIndexOf('?');
                            if (querystring >= 0) {
                                search = files[i].substr(0, querystring);
                            }
                            if ($popup('head link[href^="' + search + '"]').length < 1) {
                                $popup('<link />', {
                                    rel: 'stylesheet',
                                    type: 'text/css',
                                    href: files[i]
                                }).appendTo('head');
                            }
                        }
                    }

                    include_js = response.xhr.getResponseHeader('X-Include-JS');
                    if (include_js != null) {
                        files = include_js.split(',');
                        for (i = 0; i < files.length; i++) {
                            search = files[i];
                            querystring = files[i].lastIndexOf('?');
                            if (querystring >= 0) {
                                search = files[i].substr(0, querystring);
                            }
                            if ($popup('script[src^="' + search + '"]').length < 1) {
                                $popup.getScript(files[i]);
                            }
                        }
                    }
                },
    			ajaxContentAdded: function() {
                    $popup('form', this.content).submit(function(e) {
    					e.preventDefault();
    					$popup('p.error', this).remove();
    					data = $popup(this).serialize();
    					$popup('input, select, textarea, button', this).prop('readonly', true).prop('disabled', true).addClass('disabled');
    					$popup('div.row.controls', this).append('<p class="progress">Submitting...</p>');
    					$popup.post($popup(this).attr('action') + '?ajax=1', data, function(response) {
    						if (response.Status == 'OK') {
                                if (response.ThankYouURL != null) {
                                    window.location.href = response.ThankYouURL;
                                }
                                else {
                                    $popup('div.popup div.form-content').empty().append(response.ThankYouMessage);
                                }
    						}
    						else {
    							form = $popup('div.popup form');
    							$popup('input, select, textarea, button', form).prop('readonly', false).prop('disabled', false).removeClass('disabled');
    							$popup('div.row.controls > p', form).remove();
    							for (i = 0; i < response.Errors.length; i++) {
    								$popup('*[name=' + response.Errors[i].Name + '], ul.optionset[id$=' + response.Errors[i].Name + ']', form).after('<p class="error">' + response.Errors[i].Message + '</p>');
    							}
    						}
    					}, 'json');
    				});
    			}
    		}
    	});
    });
});
